<?php

use App\Http\Controllers\Api\Agama28Controller;
use App\Http\Controllers\api\Detaildata28Controller;
use App\Http\Controllers\api\User28Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// route::resource('/agama28', Agama28Controller::class);
Route::get('/agama28', [Agama28Controller::class, 'index28'])->name('agama28.index');
Route::post('/agama28', [Agama28Controller::class, 'store28'])->name('agama28.store');
Route::get('/agama28/{id}', [Agama28Controller::class, 'show28'])->name('agama28.show');
Route::put('/agama28/{id}', [Agama28Controller::class, 'update28'])->name('agama28.update');
Route::delete('/agama28/{id}', [Agama28Controller::class, 'destroy28'])->name('agama28.destroy');

// route::resource('/detaildata28', Detaildata28Controller::class);
Route::get('/detaildata28', [Detaildata28Controller::class, 'index28'])->name('detaildata28.index');
Route::post('/detaildata28', [Detaildata28Controller::class, 'store28'])->name('detaildata28.store');
Route::get('/detaildata28/{id}', [Detaildata28Controller::class, 'show28'])->name('detaildata28.show');
Route::put('/detaildata28/{id}', [Detaildata28Controller::class, 'update28'])->name('detaildata28.update');
Route::delete('/detaildata28/{id}', [Detaildata28Controller::class, 'destroy28'])->name('detaildata28.destroy');

// route::resource('/user28', User28Controller::class);
Route::get('/user28', [User28Controller::class, 'index28'])->name('user28.index');
Route::post('/user28', [User28Controller::class, 'store28'])->name('user28.store');
Route::get('/user28/{id}', [User28Controller::class, 'show28'])->name('user28.show');
Route::put('/user28/{id}', [User28Controller::class, 'update28'])->name('user28.update');
Route::delete('/user28/{id}', [User28Controller::class, 'destroy28'])->name('user28.destroy');

Route::put('/user28/update/image/{id}', [User28Controller::class, 'editimage28'])->name('user28.editimage');
Route::put('/user28/update/password/{id}', [User28Controller::class, 'editpassword28'])->name('user28.editpassword');

// detail
// route::resource('/detaildata28', Detaildata28Controller::class);
