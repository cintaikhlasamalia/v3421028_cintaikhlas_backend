@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-6">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="/img/{{ $user['foto'] }}"
                                alt="User profile picture" style="width: 200px; height: 200px">
                        </div>

                        <h3 class="profile-username text-center">{{ $user['name'] }}</h3>
                        <p class="text-muted text-center">{{ $user['email'] }}</p>
                        <form action="/user28/{{ $user['id'] }}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary">Delete user</button>
                        </form>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">My profile</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong>Name</strong>
                        <p class="text-muted">
                            {{ $user['name'] }}
                        </p>
                        <hr>
                        <strong>Email</strong>
                        <p class="text-muted">
                            {{ $user['email'] }}
                        </p>
                    </div>
                </div>

                @if ($detail != null)
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">More</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <strong>Agama</strong>
                            <p class="text-muted">
                                {{ $agama['nama_agama'] }}
                            </p>
                            <hr>
                            <strong>Alamat</strong>
                            <p class="text-muted">
                                {{ $detail['alamat'] }}
                            </p>
                            <hr>
                            <strong>Tempat lahir</strong>
                            <p class="text-muted">
                                {{ $detail['tempat_lahir'] }}
                            </p>
                            <hr>
                            <strong>Tanggal lahir</strong>
                            <p class="text-muted">
                                {{ $detail['tanggal_lahir'] }}
                            </p>
                            <hr>
                            <strong>Umur</strong>
                            <p class="text-muted">
                                {{ $detail['umur'] }}
                            </p>
                            <hr>
                            <strong>Foto ktp</strong>
                            <p class="text-muted">
                                <img src="/img/{{ $detail['foto_ktp'] }}" alt="error"
                                    style="width: 300px; height: 200px">
                            </p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
