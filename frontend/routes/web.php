<?php

use App\Http\Controllers\Agama28Controller;
use App\Http\Controllers\Auth28Controller;
use App\Http\Controllers\User28Controller;
use App\Http\Controllers\Detaildata28Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// auth
Route::get('/login28', [Auth28Controller::class, 'login28'])->name('auth28.login');
Route::get('/register28', [Auth28Controller::class, 'register28'])->name('auth28.register');
Route::get('/logout28', [Auth28Controller::class, 'logout28'])->name('auth28.logout');

Route::post('/login28', [Auth28Controller::class, 'loginProcess28'])->name('auth28.loginProcess');
Route::post('/register28', [Auth28Controller::class, 'registerProcess28'])->name('auth28.registerProcess');

Route::middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::middleware('admin')->group(function () {
        // agama
        Route::get('/agama28', [Agama28Controller::class, 'index28'])->name('agama28.index');
        Route::get('/agama28/create', [Agama28Controller::class, 'create28'])->name('agama28.create');
        Route::post('/agama28', [Agama28Controller::class, 'store28'])->name('agama28.store');
        Route::get('/agama28/{id}', [Agama28Controller::class, 'show28'])->name('agama28.show');
        Route::get('/agama28/{id}/edit', [Agama28Controller::class, 'edit28'])->name('agama28.edit');
        Route::put('/agama28/{id}', [Agama28Controller::class, 'update28'])->name('agama28.update');
        Route::delete('/agama28/{id}', [Agama28Controller::class, 'destroy28'])->name('agama28.destroy');

        // user
        Route::get('/user28', [User28Controller::class, 'index28'])->name('user28.index');
        Route::get('/user28/create', [User28Controller::class, 'create28'])->name('user28.create');
        Route::post('/user28', [User28Controller::class, 'store28'])->name('user28.store');
        Route::get('/user28/{id}', [User28Controller::class, 'show28'])->name('user28.show');
        Route::get('/user28/{id}/edit', [User28Controller::class, 'edit28'])->name('user28.edit');
        Route::put('/user28/{id}', [User28Controller::class, 'update28'])->name('user28.update');
        Route::delete('/user28/{id}', [User28Controller::class, 'destroy28'])->name('user28.destroy');
    });

    // my
    Route::get('/myprofile28', [User28Controller::class, 'myprofile28'])->name('user28.myprofile');
    Route::put('/myprofile28/edit/image/{id}', [User28Controller::class, 'editimage28'])->name('user28.editimage');
    Route::put('/myprofile28/edit/password/{id}', [User28Controller::class, 'editpassword28'])->name('user28.editpassword');

    // detail
    Route::get('/detaildata28', [Detaildata28Controller::class, 'index28'])->name('detaildata28.index');
    Route::get('/detaildata28/create', [Detaildata28Controller::class, 'create28'])->name('detaildata28.create');
    Route::post('/detaildata28', [Detaildata28Controller::class, 'store28'])->name('detaildata28.store');
    Route::get('/detaildata28/{id}', [Detaildata28Controller::class, 'show28'])->name('detaildata28.show');
    Route::get('/detaildata28/{id}/edit', [Detaildata28Controller::class, 'edit28'])->name('detaildata28.edit');
    Route::put('/detaildata28/{id}', [Detaildata28Controller::class, 'update28'])->name('detaildata28.update');
    Route::delete('/detaildata28/{id}', [Detaildata28Controller::class, 'destroy28'])->name('detaildata28.destroy');
});
