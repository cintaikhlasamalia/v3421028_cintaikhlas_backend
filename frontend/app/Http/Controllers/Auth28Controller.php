<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Auth28Controller extends Controller
{
    // views //
    public function login28()
    {
        return view('auth.login');
    }

    public function register28()
    {
        return view('auth.register');
    }

    // process //
    public function registerProcess28(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
        ]);

        $login = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'foto' => $request->foto,
        ]);

        if ($request->hasFile('foto')) {
            $request->file('foto')->move('img/', $request->file('foto')->getClientOriginalName());
            $login->foto = $request->file('foto')->getClientOriginalName();
            $login->save();
            return redirect('/login28');
        }

        return redirect('/register28')->with('success', 'register success');
    }

    public function loginProcess28(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        $login = Auth::attempt(['email' => $request->email, 'password' => $request->password]);

        if ($login) {
            if (Auth::user()->is_aktif) {
                return redirect('/');
            }
            Auth::logout();
            return redirect('/login28')->with('error', 'Belum di approve');
        }
        return redirect('/login28')->with('error', 'Email or password wrong');
    }

    public function logout28()
    {
        Auth::logout();
        return redirect('/login28')->with('success', 'Logout success');
    }
}
