<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class Agama28Controller extends Controller
{
    public function index28()
    {
        $response = Http::get(env('API_URL') . '/agama28')->json();
        return view('agama.agama', [
            'response' => $response
        ]);
    }

    public function store28(Request $request)
    {

        Http::post(env('API_URL') . '/agama28', [
            'nama_agama' => $request->nama_agama,
        ]);

        return redirect('/agama28')->with('status', 'error');
    }

    public function update28(Request $request, $id)
    {
        Http::put(env('API_URL') . '/agama28/' . $id, [
            'nama_agama' => $request->nama_agama
        ]);

        return redirect('/agama28');
    }

    public function destroy28($id)
    {
        Http::delete(env('API_URL') . '/agama28/' . $id);

        return redirect('/agama28');
    }
}
