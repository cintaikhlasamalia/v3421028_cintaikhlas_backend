<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class User28Controller extends Controller
{
    public function myprofile28()
    {
        $agama = Http::get(env('API_URL') . '/agama28')['data'];
        return view('user.myprofile', [
            'agamas' => $agama
        ]);
    }

    public function editimage28(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->foto = $request->foto;

        if ($request->hasFile('foto')) {
            $foto_name = $request->file('foto')->getClientOriginalName();
            $request->file('foto')->move('img/', $request->file('foto')->getClientOriginalName());
        }

        Http::put(env('API_URL') . '/user28/update/image/' . $id, [
            'foto' => $foto_name
        ]);

        return redirect('/myprofile28');
    }

    public function editpassword28(Request $request, $id)
    {
        Http::put(env('API_URL') . '/user28/update/password/' . $id, [
            'password' => $request->password
        ]);

        return redirect('/myprofile28');
    }

    public function index28()
    {
        $user = Http::get(env('API_URL') . '/user28')['data'];
        return view('user.user', [
            'users' => $user,
            'no' => 1,
        ]);
    }

    public function show28($id)
    {
        $user = Http::get(env('API_URL') . '/user28/' . $id)['data'];
        $detail = Http::get(env('API_URL') . '/detaildata28/' . $id)['data'];
        $agama = Http::get(env('API_URL') . '/agama28/' . $id)['data'];

        return view('user.userdetail', [
            'user' => $user,
            'detail' => $detail,
            'agama' => $agama,
        ]);
    }

    public function destroy28($id)
    {
        Http::delete(env('API_URL') . '/user28/' . $id);
        return redirect('/user28');
    }

    public function update28($id)
    {
        Http::put(env('API_URL') . '/user28/' . $id);
        return redirect('/user28');
    }
}
